/**
 * Input la 10000/ 1ngay 
 *      so ngay lam
 * output= 100000 * so ngay lam
 * progress tao 1 bien gia value cua input
 * tao bien tinh tien luong
 * t bien gan tien luong co the span
 */

 var luong1Ngay = 100000;
 var soNgayLam = 30;
 
 var luong = 0;
 
 
 luong = luong1Ngay * soNgayLam;
 console.log('Lương nhân viên =', luong);
 
 
 
 
 /*
 Tính giá trị trung bình
 
 Giá trị 5 số thực: 10, 20, 30, 40, 50
 
 Bước 1: Tạo biến numb1, numb2, numb3, numb4, numb5
 Bước 2: Gán giá trị cho numb1, numb2, numb3, numb4, numb5
 Bước 3: Tính giá trị của tổng 5 số người dùng nhập chia cho 5
 Bước 4: In kết quả ra console
 
 Kết quả giaTriTrungBinh 
 */ 
 var numb1 = 10;
 var numb2 = 20;
 var numb3 = 30;
 var numb4 = 40;
 var numb5 = 50;
 
 giaTriTrungBinh = (numb1 + numb2 + numb3 + numb4 + numb5) / 5;
 
 console.log('giaTriTrungBinh =', giaTriTrungBinh);
 
 
 /*
 Quy đổi tiền
 
 Giá USD hiện nay = 23.500VND
 
 Bước 1: Tạo biến giaVND, soluongUSD, giaUSD
 Bước 2: Gán giá trị cho giaVND, soluongUSD, giaUSD
 Bước 3: Quy đổi số lượng USD sang VND 
 Bước 4: In kết quả theo biểu mẫu ra console
 
 Kết quả USD người dùng nhập đổi sang VND
 */
 var giaVND = 23500;
 var soluongUSD = 2;
 
 var gia2USD = 0;
 
 gia2USD = giaVND * soluongUSD;
 console.log('2 USD =', gia2USD);
 
 
 /*
 Tính diện tích, chu vi hình chữ nhật 
 
 Cho chiều dài là 5
 Cho chiều rộng là 3
 
 Bước 1: Tạo biến chieuDai, chieuRong, chuVi, dienTich
 Bước 2: Gán giá trị cho chieuDai, chieuRong
 Bước 3: Tính công thức diện tích và chu vi
 Bước 4: In 2 kết quả (chu vi và diện tích) ra console
 
 Kết quả chuvi dienTich (chu vi và diện tích)
 
 */ 
 var chieuDai = 5;
 var chieuRong = 3;
 
 var chuVi = 0;
 var dienTich = 0;
 
 chuVi = (chieuDai + chieuRong) * 2;
 
 dienTich = chieuDai * chieuRong;
 
 console.log('Chu vi =', chuVi);
 console.log('Diện tích =', dienTich);
 
 
 /*
 Tính tổng 2 ký số
 
 Số nguyên n có 2 ký số (50)
 5 + 0 = 50
 
 Bước 1: Tạo biến n, hangChuc, donVi, tong2KySo
 Bước 2: Gán giá trị cho n
 Bước 3: Tách số hàng chục theo công thức hangChuc = Math.floor (n/10)
 Bước 4: Tác số hàng đơn vị theo công thức donVi = Math.floor (n % 10);
 Bước 5: In kết quả tong2KySo ra console
 
 Kết quả tong2KySo (tổng 2 ký số)
 
 */
 var n = 50;
 
 var tong2KySo = 0;
 
 var hangChuc = Math.floor (n / 10);
 var donVi = Math.floor (n % 10);
 
 tong2KySo = hangChuc + donVi;
 
 console.log('Tổng 2 ký số =', tong2KySo);